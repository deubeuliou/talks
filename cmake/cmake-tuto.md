% CMake tutorial
% David Wagner

# Introduction

## What is CMake?

*<http://cmake.org>*

CMake is not a build system. It is a build system generator. As such, it can generate:

- UNIX Makefiles (used with `make`);
- Visual Studio project files (`.sln` files)
- Ninja build files
- NMake Makefiles
- ...

It has its own scripting language which is fairly easy to learn. CMake mainly covers the need of
C/C++ projects.

## Pre-requisites for following these slides

Understand basic build system principles:

- target dependencies;
- compilation pipeline;
- build steps.

Understand portability concerns at some extent.

Understand that - especially for C/C++ - the Makefiles are as important as the code.

## Glossary and conventions

- **CMakefiles**: files written in the "CMake" syntax, used to generate a build system. They are
usually called **`CMakeLists.txt`** or have a ".cmake" extension;
- **developer**: the person writing the CMakefiles (and probably the project's source code as
well);
- **builder**: the person using CMake to actually build a project (he is not writing any
CMakefile);
- **generation time**: when cmake is run to generate the build system (before build time).

CMake variables are written in `UPPER_SNAKE_CASE`; commands are written in `lower_camel_case`;
keyword-arguments within commands are written in `UPPER_SNAKE_CASE`.

This tutorial assumes CMake 3.2.

## Plan

- Overview of features
- What you need to know for very simple projects
- How CMake integrates with the outside world
- Helfpul knowledge when compiling an existing CMake project

(Not included in the slides: a case study of the [Parameter
 Framework](https://github.com/01org/parameter-framework)'s CMakefiles)

# Features and concepts

## Resources

*`man cmake-buildsystem` or <https://cmake.org/cmake/help/v3.2/manual/cmake-buildsystem.7.html>*.

These slides will not cover every CMake command and features that exists. **You
should explore by yourself** by looking at the various manpages or the online doc
(<https://cmake.org/documentation/>) - which includes a Wiki, a FAQ and a tutorial.

## Show me the code!

This is what a **complete, standalone** CMakefile looks like:

```{.cmake}
cmake_minimum_required(VERSION 3.2.2)
project(tutorial)

if(WIN32)
    message(FATAL_ERROR "Windows not supported yet")
endif()

find_package(Threads REQUIRED)

option(FANCY "Add some kittens." ON)
set(CMAKE_CXX_STANDARD 14)

set(TUTO_VERSION "0.1.15")
configure_file(version.h.in "${CMAKE_CURRENT_BINARY_DIR}/version.h")

add_subdirectory(component) # contains a library named 'component'

add_executable(tutobin src/main.cpp)
target_compile_definitions(tutobin $<$<BOOL:${FANCY}>:ENABLE_FANCY>)
target_link_libraries(tutobin
                      PRIVATE component Threads::Threads)

install(TARGETS tutobin RUNTIME DESTINATION bin)
```

## Targets

Target kinds include:

- actual compilation targets (e.g. **executables**, **static** or **shared libraries**);
- custom targets (e.g. for generating documentation or other non-binary targets);
- imported (pseudo-targets that wrap binaries found on the system);
- **interface** targets (another kind of pseudo-targets that only serve as declaring compilation
requirements) - primarily used for header-only libraries.

Example:

```{.cmake}
add_library(mylib SHARED foo.c bar.c) # shared lib called 'mylib'
target_include_directories(mylib PUBLIC lib/include) # declare its includes

add_executable(myexe one.c two.c) # executable called 'myexe'
target_include_directories(myexe PRIVATE exe/include)

target_link_libraries(myexe mylib) # link 'myexe' against 'mylib'
```

## Variables and Properties

CMake declares a lot of **variables**, some of which are up to the builder to set/override (e.g.
the place where to install the built targets), some to be decided by the developer (such as the C++
standard to be used, some CMake behaviours). You should resist the temptation to use the kind
you're not supposed to.

Some variables are purely informational (read-only). Information about the platform, about CMake or
compiler capabilities, ...

**Properties** are like variables but they affect or describe specific items: source files, targets,
tests (used by ctest), and more... **They are the low-level information that CMake uses to generate
the build system.**

Properties can be:

- initialized by CMake based on specific variables;
- set through helper commands (e.g. `set_compile_definitions`);
- set directly via `set_property` or similar commands.

## Transitive usage requirements

Targets may declare **usage requirements** defining how to be used in making another target (e.g.
linker flags or include directories). The are set using the same command as the ones used for
compiling the target in the first place. So, how does CMake tell them apart?

Commands controlling how targets are built have 3 modes:

- `PRIVATE` only sets how the target is **built**
- `INTERFACE` only sets how the target is **used**
- `PUBLIC` does **both**.

It is important to understand when to use each mode.

## Single- and multi-configuration build systems

Some build system may manage different build configurations at the same time (e.g. Visual Studio)
whereas other only handle one at a time (Make). In the former case, the build configuration is
decided at **build time** (`cmake --build <path> --config Xyz`) - in the latter case, it is decided
at **generation time** (`cmake -DCMAKE_BUILD_TYPE=Xyz`).

They both have their own advantages and inconvenients.

CMake has 3 built-in configuration types:

- **Debug**;
- **Release**;
- **RelWithDebInfo** (release with debug information);
- **MinSizeRel** (minimum-size release).

(Note that single-configuration build systems may also be created without passing a configuration)

## Some associated tools

- **CTest** is a test runner and reporter.
- **CPack** is able to create archives and installation packages in various formats.
- **ccmake** is the CMake curses (Command-line UI) wizard.
- **cmake-gui** is the CMake wizard GUI.
- There are **vim plugins** for [CMake completion](https://github.com/richq/vim-cmake-completion)
and for up-to-date [CMake syntax highlighting](https://github.com/pboettch/vim-cmake-syntax).

# Basic CMake

## My first CMakefile

main.cpp:
```{.cpp}
#include <iostream>
int main(void) {
    std::cout << "Hello, CMake\n";
    return 0;
}
```

CMakeLists.txt:
```{.cmake}
cmake_minimum_required(VERSION 3.2.0)
project(tutorial)

add_executable(tuto main.cpp)
```

Out-of-tree build: configuration, compilation and run
```{.sh}
$ pwd
/home/user/devel/tuto
$ mkdir ../build && cd ../build
$ cmake ../tuto
[...]
$ make
[...]
$ ./tuto
Hello, CMake
$
```

## Another example: a lib and two executables

CMakeLists.txt:
```{.cmake}
cmake_minimum_required(VERSION 3.2.0)
project(tutorial)

# defines an imported Threads::Threads library
find_package(Threads REQUIRED)

add_library(protocol STATIC protocol/src/api.cpp protocol/src/impl.cpp)
target_include_directories(protocol PUBLIC protocol/include)
target_link_libraries(protocol PRIVATE Threads::Threads)

add_executable(client client/src/main.cpp)
add_executable(server server/src/main.cpp)
```

## Libraries and executables (binary targets)

```{.cmake}
add_library(<name> [STATIC|SHARED]
            source1 [source2 ...])
add_executable(<name>
        source1 [source2 ...])
```

This is how **commands** are described in the manual. They are referred to by their name: the
command above is called `add_library()`. Commands often have several variants, in which case the
variant is mentioned in parentheses, such as in `some_command(SOME_VARIANT)`.

`[arguments]` are optional while `<arguments>` are mandatory. `UPPER_SNAKE_CASE_ARGUMENTS` are
keywords and are to be types literally (they are options or positional arguments that introduce a
type of arguments).


## Compilation options

Need to add compilation options on you targets? CMake has several solutions for you, including:

- using global variables;
- using the eponymous `target_compile_options()` function;
- using properties (on the file scope, the target scope or the directory scope).

## Compilation options, method 1: global variables

*`man cmake-variables` or <https://cmake.org/cmake/help/v3.2/manual/cmake-variables.7.html>*.

The `CMAKE_C_FLAGS` (resp. `CMAKE_CXX_FLAGS`) variable contains compilation flags for C (resp. C++)
files.  It is not unusual for CMake to define language-flavoured variables. They are referred to in
the documentation as `CMAKE_<LANG>_...`.

Usage example:

```{.cmake}
if(COVERAGE) # COVERAGE could be a CMake "option" declared by the developer (more on this later)
    list(APPEND CMAKE_CXX_FLAGS --coverage)
endif()
```

### Drawbacks

- CMake has already set it to a default value - take care not to overwrite them (use `list(APPEND)`
  instead of `set`).
- You may want to use different compilation options for different parts of your project. A global
  variable won't let you do that.
- You can't use **generator expressions**.
- Doesn't allow for transitive usage requirement.
- Not **portable**.

## Compilation options, method 2: `target_compile_options`

*`man cmake-commands` or <https://cmake.org/cmake/help/v3.2/command/target_compile_options.html>*

Usage example: `target_compile_options(myTarget PRIVATE -Wall -Wextra)`.

You can use generator expressions and using `target_compile_options(PUBLIC|INTERFACE)`, you can
specify usage requirements. However, **this is still not portable**.

CMake has another command, specifically for compilation definitions (things like `-DVAR=value`):
`target_compile_definitions`. Usage example: `target_compile_definitions(myTarget PUBLIC VAR=value
SOME_FEATURE)`. This one **is portable**.

## Compilation options, method 3: properties

*`man cmake-properties` or <https://cmake.org/cmake/help/v3.2/manual/cmake-properties.7.html>*

Usage examples:

- `set_target_properties(myTarget PROPERTIES POSITION_INDEPENDENT_CODE TRUE)`
- `set(CMAKE_CXX_STANDARD 14)` (technically not a property but the basis for target properties)

Some of these properties **are portable** (the actual changes to the compilation chain or options
is abstracted and depends on the OS and the generated build system - it may even be ignored if not
applicable). Counter-example: setting `COMPILE_OPTIONS` directly is not portable (cf. previous
slide).

## Generating files from templates

```{.cmake}
configure_file(<input> <output>
               [COPYONLY] [ESCAPE_QUOTES] [@ONLY]
               [NEWLINE_STYLE [UNIX|DOS|WIN32|LF|CRLF] ])
```

The `configure_file` command generates a file based on a **template**. This happens **generation
time**. CMake looks for patterns like `${variable}` or `@variable@` and replaces them with the
value of the variable.

The `add_custom_command()` and `add_custom_target()` commands are also worth mentioning.

## Declare and run tests using CTest

CTest comes a both a CMake module (more on modules later) and a utility.

Usage example:

```{.cmake}
include(CTest)
...
add_executable(foobar ...)
add_test(NAME my_foobar_test COMMAND foobar --some-argument)
```

The command argument may also be an arbitrary shell command. By default, `ctest` expects the
command to return 0 on success and any other return value is a failure. See documentation for
`add_test()` and properties on tests for more details.

After the project has been compiled, tests may be run using `ctest` or by building the target named
`test` (e.g. `make test`). However, **the test target does not depend on the executables** which
means that they won't be automatically rebuilt.

CTest can also perform **coverage measurement** (e.g. using gcov) or perform memory safety analysis
(using valgrind).

## Install your targets and resources

```{.cmake}
# The following signatures have been simplified (some features removed)
# and some variants have been omitted.

install(TARGETS targets...
        [[ARCHIVE|LIBRARY|RUNTIME]
         [DESTINATION <dir>]
         [CONFIGURATIONS [Debug|Release|...]]
        ])

install(<FILES|PROGRAMS> files... DESTINATION <dir>
        [CONFIGURATIONS [Debug|Release|...]]
        [RENAME <name>])
```

The destinations are relative to the installation directory (by default on Linux: `/usr/local`)
which can be specified using the `CMAKE_INSTALL_PREFIX` variable at generation time.

The definition of archive, library and runtime may vary depending on the OS. Here's a typical usage
on Linux:

```{.cmake}
install(TARGETS myLibrary
        LIBRARY DESTINATION lib
        RUNTIME DESTINATION bin)
install(FILES
        include/myHeader.h
        DESTINATION "include/myLibrary")
```

## Provide build options to the user

If your project has optional features or you want to give the builder the ability to control how
the build happens (e.g. C/C++ compiler flags, verbosity, ...), CMake lets you declare those.

Basically, they will be stored as CMake variables but they have more tooling support (e.g. with
wizards like `ccmake` and `cmake-gui` or with `cmake -LH`).

Usage example:

```{.cmake}
option(COVERAGE "Build with coverage support" OFF) # default value is OFF
```

The builder can then choose to activate coverage measurement with `cmake -DCOVERAGE=ON <path to
build or sources>`.

## Multi-directories projects

Use `add_subdirectory(some/path)` to add targets and other instructions from several CMakefiles.

Directory structure example:

```
project/
    CMakeLists.txt
    myexe/
        CMakeLists.txt
    include/
    mylib1/
        CMakeLists.txt
    mylib1/
        CMakeLists.txt
```

# CMake's syntax

## Control flow

```{.cmake}
if(<condition>)
elseif(<condition>)
else()
endif()
```

`<condition>` can either be:

- a constant (e.g. `if(TRUE)`);
- a variable name (e.g. `if(VAR)`);
- a string (e.g. `if(${VAR})`);

or it can use a more complex expression using unary or binary operators, e.g.: `if((CMAKE_GENERATOR
MATCHES "Visual Studio .*") AND (CMAKE_GENERATOR STRGREATER "Visual Studio 14 2015"))`.

## Functions

```{.cmake}
function(<name> [arg1 [arg2 [arg3 ...]]])
  command1(ARGS ...)
  command2(ARGS ...)
  ...
endfunction()
```

Functions open a new variable scope.

## Generator expressions

*`man cmake-generator-expressions` or
<https://cmake.org/cmake/help/v3.2/manual/cmake-generator-expressions.7.html>*.

They look `$<LIKE:this...>` or `$<LIKE_THIS>`.

Generator expressions are evaluated at **generation time**. They have a name and arguments. There
is a fixed set of generator expressions (i.e. names) - see the doc for the full list. They can be
nested (i.e. a generator expression can be used to generate the name or the arguments of another
expression.

Example of some commands:

```{.cmake}
$<BOOL:...> # 1 if ... is true, 0 otherwise
$<0:...> # empty string
$<1:...> # the value of ...
$<TARGET_FILE_NAME:target> # name of the target's main file (e.g. "toto.exe")
$<MAKE_C_IDENTIFIER:...> # content of ... converted to a C identifier
$<CONFIG> # The current build configuration (aka build type)
```

# Reuse, reuse, reuse

## CMake modules (aka "I want moar!")

*`man cmake-modules` or <https://cmake.org/cmake/help/v3.2/manual/cmake-modules.7.html>*

CMake comes with a lot of helpers for tasks that are not basic build system requirements but that
developers often need to do. For instance:

- Checking if a C type exists and what its size is.
- Check if a compile supports this or that flag.
- Make some boilerplate for external utilities (bindings generator, GUI test tools...).
- Make some options depend on one another.
- ...

Before making anything complex, you should first **check whether a CMake module can help you**.

Usage examples:

```{.cmake}
include(GenerateExportHeader)
include(CMakeDependentOption)
```

(Note that the `include()` command may also be used to include arbitrary files verbatim in the
current CMakefile)

## Package modules

Packages are implemented as modules and are documented on the same manual but they can be used
through the **`find_package()`** command. A module named `FindXyz` is a package and is used with
`find_package(Xyz)`.

```{.cmake}
find_package(<package> [version] [EXACT] [QUIET] [MODULE]
             [REQUIRED] [[COMPONENTS] [components...]]
             [OPTIONAL_COMPONENTS components...]
             [NO_POLICY_SCOPE])
```

The `find_package` command loads information from external projects (targets, tools, ...). CMake
comes with a set of packages that you can import (e.g. libxml2, boost, doxygen, wget, ...) or you
could use packages defined by 3rd parties (more on that later).

Usage Example:

```{.cmake}
# defines an imported Threads::Threads library
find_package(Threads REQUIRED)

target_link_libraries(myLibrary PRIVATE Threads::Threads)
```

## Interface targets

A header-only library has, by definition, no source files to be compiled; however, it can still be
regarded as a target for the purpose of usage requirements transmission. Its usage requirements
consists of include directories, compilation options and maybe other CMake properties.

CMake **interface libraries** can be used to implement such libraries (they have other uses too):

```{.cmake}
add_library(<name> INTERFACE)
```

Interface libraries may even be installed and exported (more on this in a moment).

# Deploy your own packages

## Installation packages vs. CMake packages

When talking about packages in the context of CMake, it can refer to two different things:

- distribution and installation packages.
- CMake discovery and configuration modules (aka package modules).

The first is used to **install your project** on a destination machine while the second is used by
other developers using CMake to benefit from your project using **`find_package()`**.

## Installation packages, intro

*`man cpack` or <https://cmake.org/cmake/help/v3.2/manual/cpack.1.html>*

CPack is a tool associated with a CMake **module** that lets developer provide **instructions and
metadata** for creating **distribution and installation packages** (e.g. windows installers, debian
packages, compressed archives, ...).

The output of `cpack --help` lists available generators:

```
Generators
  DEB        = Debian packages
  IFW        = Qt Installer Framework
  NSIS       = Null Soft Installer
  NSIS64     = Null Soft Installer (64-bit)
  RPM        = RPM packages
  7Z         = 7-Zip file format
  STGZ       = Self extracting Tar GZip compression
  TBZ2       = Tar BZip2 compression
  TGZ        = Tar GZip compression
  TXZ        = Tar XZ compression
  TZ         = Tar Compress compression
  ZIP        = ZIP file format
```

Enabling package creation is simply done by adding `include(CPack)` in your CMakefiles. After
compiling your project, call `cpack -G <generator>`. The package will include all **installed**
files (the ones that would be installed by the CMake `install` commands).

## Installation packages, configuration

CPack provides a lot of configuration variables for package metadata. Here are some of them:

```
CPACK_PACKAGE_VERSION
CPACK_PACKAGE_DESCRIPTION_SUMMARY
CPACK_PACKAGE_DESCRIPTION_FILE
CPACK_PACKAGE_FILE_README
CPACK_PACKAGE_VENDOR
CPACK_PACKAGE_CONTACT
CPACK_RESOURCE_FILE_LICENSE
CPACK_DEBIAN_PACKAGE_HOMEPAGE
```

Some variables are generator-specific. Take a look at the "cpack" section of the *cmake-modules*
manpage.

## CMake package modules, intro

*`man cmake-packages` or <https://cmake.org/cmake/help/v3.2/manual/cmake-packages.7.html>*.

As explained before, CMake lets you easily use 3rd-party libraries and utilities. It also lets you
deploy your own package so that others may take advantage of `find_package()` to use your project.

Creating such packages is not trivial - the documentation is a must-read.

## CMake package modules, prerequisites

Your CMake targets need to declare their usage requirements properly and thouroughly:

```{.cmake}
# The source tree looks like this:
# impl.cpp
# impl.h
# include/
#   MyProject/
#     iface.h
add_library(lib SHARED impl.cpp) # impl.cpp includes "impl.h" and <MyProject/iface.h>
target_include_directories(lib PUBLIC include PRIVATE .)
# target_compile_definitions(...), etc.
install(TARGETS lib EXPORT MyProjectTargets
        LIBRARY DESTINATION lib
        RUNTIME DESTINATION bin # for windows
        INCLUDES DESTINATION include)
install(FILES include/MyProject/iface.h
        DESTINATION include/MyProject)
```

## CMake package modules, example

The following is a **simplified** example: it is sufficient for a simple project but has some
limitations.

```{.cmake}
# CMakeLists.txt

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
    "${CMAKE_CURRENT_BINARY_DIR}/MyProjectConfigVersion.cmake"
    COMPATIBILITY SameMajorVersion)

# Generates and installs a CMake file containing code to import targets from
# the installation tree into another project.
install(EXPORT MyProjectTargets DESTINATION lib/cmake/MyProject
    FILE MyProjectTargets.cmake
    NAMESPACE MyProject::)

install(FILES
    MyProjectConfig.cmake
    "${CMAKE_CURRENT_BINARY_DIR}/MyProjectConfigVersion.cmake"
    DESTINATION lib/cmake/MyProject)
```

```{.cmake}
# MyProjectConfig.cmake (this is the file find_package() will look for)

include("${CMAKE_CURRENT_LIST_DIR}/MyProjectTargets.cmake")
```

## Export headers, intro

When writing a C/C++ library, it is a good practice to **hide all symbols except for those
part of the API**. This is done differently on Linux (through symbol visibility) and on Windows
(export/import DLL symbol mechanism) but there are tricks to hide that complexity.

CMake provides a module implementing such a trick: `GenerateExportHeader`, which declares the
`generate_export_header()` function.

This function produces a header provinding two macros: one for marking symbols as part of the API
and one whose presence indicates the stage (present: compiling our library; absent: compiling
another project against our library). The `generate_export_header()` function adds the latter macro
to the target's compilation definition to indicate the first stage.

The developer is responsible for installing the header properly.

## Export headers, example

```{.cmake}
# CMakeLists.txt
add_library(myLibrary myLibrary.c)
include(GenerateExportHeader)
generate_export_header(myLibrary) # Important: this must be called *after* declaring the target.
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/myLibrary_export.h)
```

(Note that `generate_export_header()` has a lot of **optional arguments** to alter its default
behaviour.)

This generate a header called `myLibrary_export.h` which declares a `MYLIBRARY_EXPORT` macro. Use
this macro to tag symbols that are to be exported/made public, e.g.:

```{.cpp}
#include "myLibrary_export.h"
...
class MYLIBRARY_EXPORT MyClass {
    ...
};
int MYLIBRARY_EXPORT myFunc(...);
```

## Componentize

A complex project is made up of several executables, libraries, groups of headers and resources.
The users of such project may not all needaall of them at the same time.

For instance, headers of project A are only needed when developing a project B using A. Utilities
and libraries built by a project may only provide extended functionality and therefore optional,
...

That's where **components** come in handy. `install()` commands take an optional `COMPONENT`
argument, indicating which component the installed targets or files belong to. This information is
then used for:

- Creating per-component installation packages with CPack;
- Componentizing CMake package modules (`find_package()` has an optional `COMPONENTS` argument).

## Components, how-to

```{.cmake}
install(TARGETS myLib
        DESTINATION lib
        COMPONENT runtime)
install(FILES
        include/foo.h
        ...
        DESTINATION includes/myLibrary
        COMPONENT dev)
```

```{.sh}
cpack -G TGZ -D CPACK_ARCHIVE_COMPONENT_INSTALL=ON
```

(Note: debian packages componentization does not work well before CMake 3.5)

# What builders should know

## Out-of-tree build

As a general rule, it is recommended to build out-of-tree in order not to mess with the version
control system.

```{.sh}
mkdir ../build && cd ../build
cmake ../src
make
# afterward, 'cmake .' or 'cmake ../src' are equivalent
```

## Build types or configurations

Builtin build types:

- (none)
- Release
- Debug
- RelWithDebInfo (Release with debug information)
- MinSizeRel (Minimum size release)

Developers may also declare their own build types and change build behaviour accordingly.

The builder may select the build type by setting the `CMAKE_BUILD_TYPE` variable: `cmake
-DCMAKE_BUILD_TYPE=Xyz .`.

## Change the C or C++ compiler

Two solutions:

```{.sh}
CC=clang CXX=clang++ cmake ...
cmake -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ ...
```

The latter form is more reliable because the former will only work the first time you invoke cmake
(you'll need to delete `CMakeCache.txt` to make it work again).

## Installation path

The default installation path is `/usr/local` on Linux, which is not writeable without admin
rights. Also, it is usually considered bad practice to install stuff outside of the control of a
package manager. However, if necessary, this path can be changed (e.g. to one's home directory)
using the `CMAKE_INSTALL_PREFIX` variable.

## Search paths

The family of `find_xyz()` commands has a complex search hierarchy, each relying on specific
variables (see the `cmake-commands` manpage for full details) but generally speaking, **setting the
`CMAKE_PREFIX_PATH` sets a starting point for finding anything**.

For instance, if you are looking for a library and you've set `CMAKE_PREFIX_PATH=/some/path`, then
CMake will look for it in priority in `/some/path/lib`; if you're looking for an include file,
CMake will look for it in priority in `/some/path/include`, and so on.

The builder may also set this variable to multiple values (`CMAKE_PREFIX_PATH='path1;path2'`); in
that case, they will all be searched in that order.
<!--
Let's say you've previously install project1 under `$HOME/local/project1` and project2
under `$HOME/local/project2` and you now want to build project3 which depends on those
two. You could write:

```{.sh}
cmake -DCMAKE_PREFIX_PATH="$HOME/local/project1;$HOME/local/project2" \
      -DCMAKE_INSTALL_PREFIX=$HOME/local/project3
```
-->
